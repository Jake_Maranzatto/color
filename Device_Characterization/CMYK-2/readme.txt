----------------
Input.txt
----------------
The data is of type 'CMYK'.
-C,M,Y,K scales in the range 0..100

----------------
Output.txt
----------------
The data is of type 'Lab'.
-L scales in 0..100
-a,b scale in -128..128 