----------------
Input.txt
----------------
The data is of type 'RGB'.
-R,G,B scales in the range 0..255

----------------
Output.txt
----------------
The data is of type 'Lab'.
-L scales in 0..100
-a,b scale in -128..128 