import math
import numpy as np
import random
from scipy.interpolate import griddata
##OUR DATA##
CMYK1Bench = './Device_Characterization/CMYK-1/Benchmark//Output.txt'
CMYK1Characterization = './Device_Characterization/CMYK-1/Reference/Output.txt'

CMYK2Bench = './Device_Characterization/CMYK-2/Benchmark/Output.txt'
CMYK2Characterization = './Device_Characterization/CMYK-2/Reference/Output.txt'

RGBBench = './Device_Characterization/RGB-1/Benchmark/Output.txt'
RGBInput = './Device_Characterization/RGB-1/Characterization/Input.txt'
RGBOutput = './Device_Characterization/RGB-1/Characterization/Output.txt'
RGBPointsToCharacterize = './Device_Characterization//RGB-1/Reference/Input.txt'
RGBCharacterization = './Device_Characterization//RGB-1/Reference/Output.txt'


#######DATA HELPER FUNCTIONS#####################

#converts a file to a list
#of data points
def fileToList(file, dim):
    data = open(file, 'r')
    ret = []
    for line in data:
        point = line.rstrip('\n').split('\t')
        sub_ret = []
        for j in range(len(point)):
            try:
                sub_ret.append(float(point[j]))
            except:
                continue
        if len(sub_ret) == dim:
            ret.append(sub_ret)
    return ret

def writeFileData(output, value):
    for val in value:
        output.write(str(val) + '\t')
    output.write('\n')

    
#returns the mean squared error over an input hypothesis
#with reference to it's benchmark
#the lower the better
def error(benchmark, hypothesis):
    maxError = 0
    errors = []
    
    bench = fileToList(benchmark, 3)
    fingerprint = fileToList(hypothesis, 3)

    sumSquares = 0
    iteration = 0
    for i, k in enumerate(zip(bench, fingerprint)):
        iteration = i
        
        b = k[0]
        f = k[1]
        
        for j in range(len(b)):
            try:
                q = (float(b[j]) - float(f[j]))**2
                sumSquares += q
                errors.append([q, b, f])
                if q > maxError:
                    maxError = q
                    ErrorExample = b
                    ErrorValue = f
            except Exception as e:
                continue
    errors.sort()
    x = errors[len(errors) - 100:]
    tot = 0
    for i in x:
        print(i[0])
        tot += i[0]
        
    #print(int(maxError), ErrorExample, ErrorValue)
    return("Total error is: " + str(sumSquares) + '\n' +
           "Average error is: " + str(sumSquares/float(iteration)))

#####################################################

#gets the euclidian distance between two colors
def distance(x, y):
    return math.sqrt(sum(([(i-j)**2 for i,j in zip(x,y)])))

#returns datapoint and its value closest to input values
def nearest(point, data, values):
    dist = None
    p = None
    value = None
    for i, location in enumerate(data):
        #print(i, len(inData), len(inValues))
        if dist == None or distance(location, point) < dist:
            dist = distance(location, point)
            p = location
            value = values[i]

    return p, value
    
#overwrites a hypothesis file that creates
#a K-nearest-neighbor hypothesis for the given
#data points and input data

#note that the output format is different to the files we were given
#but the above 'error' method is agnostic to this via the try-except block
def nearestNeighbor(datapoints, values, points, outputFile):
    output = open(outputFile, 'w')
    inData = fileToList(datapoints, 3)
    inValues = fileToList(values, 3)
    inPoints = fileToList(points, 3)
    for point in inPoints:
        value = nearest(point, inData, inValues)[1]
                
        writeFileData(output, value)

##########################################################################################
##########################################################################################

#as the naiive nearest-neighbor approach didnt work,
#we are going to try the n-dimensional parallelogram that surrounds the given point
#we need a few helper methods for this:

def nearDir(point, data, values, dimension, direction, iteration = 0):
    dist = None
    p = None
    value = None
    for i, location in enumerate(data):
        if point != location:
            x = True
            for i in range(len(point)):
                if i == dimension:
                    continue
                if point[i] != location[i]:
                    x = False
                    break
            if x == True:
                if dist == None or ( direction*point[dimension] <= direction*location[dimension] and distance(location, point) < dist ):
                    dist = distance(location, point)
                    p = location
                    value = values[i]

    #attempting to catch edge cases
    if p == None and value == None and iteration == 0:
        return nearDir(point, data, values, dimension, -1 * direction, 1)

    #if edge case is not an issue or fails:
    return (p , value)

def fourNearest(point, data, values):
    closestTuple = nearest(point, data, values)
    closestInput = closestTuple[0]
    closestOutput = closestTuple[1]

    #gets us the directions we need to look for more points.
    #eg this vector may look like [1,1,0,-1]
    near = []
    disp = [point[i] - closestInput[i] for i in range(len(point))]
    displacement = [i/abs(i) if i != 0 else -1 for i in disp]
    near = [(closestInput, closestOutput)]
    for i, direction in enumerate(displacement):
        near.append(nearDir(closestInput, data, values, i, direction))

    #attempting to catch edge cases
    if (None, None) not in near:
        return near
    return [i if i != (None, None) else (closestInput, closestOutput) for i in near]

#all data should be floats, not vectors
#here data and values should be lists of length 2
#returns a single value
#will change this to get best experimental values
def Interpolate(point, data, values):
    if not ((point <= data[0] and point >= data[1]) or (point >= data[0] and point <= data[1])):
        #return nearest([point], [[i] for i in data], [[i] for i in values])
        #print(point, data)
        None
    return (point - data[0])/(data[1] - data[0])*(values[1]-values[0]) + values[0]
    #return values[0]*(values[1]/values[0])**((point - data[0])/(data[1]-data[0]))

#does what it says on the tin
def getFirstNonSame(lst1, lst2):
    index = 0
    for i in zip(lst1, lst2):
        if i[0] == i[1]:
            index += 1
            continue
        else:
            return index

def dimInterpolate(point, inData, inValues):
    #list of data-value pairs
    points_values = fourNearest(point, inData, inValues)
    value = [0 for i in range(len(inValues[0]))]
    basis = points_values[0]
    base = basis[0]
    for pv in points_values[1:]:
        dpoint = pv[0]
        dim = getFirstNonSame(dpoint, base)
        if dim == None:
            value = basis[1]
            break
        data = [dpoint[dim], base[dim]]
        values = [pv[1][dim], basis[1][dim]]
        ret =  Interpolate(point[dim], data, values)
        value[dim] = ret
        
    #attempting to catch edge cases
    #could return 0 in cases where we have
    for i in range(len(value)):
        if value[i] == 0:
            value[i] = basis[1][i]
    return value

def x4_Interpolation(datapoints, values, points, outputFile):
    output = open(outputFile, 'w')
    inData = fileToList(datapoints, 3)
    inValues = fileToList(values, 3)
    inPoints = fileToList(points, 3)
    
    for point in inPoints:
            #near = list(nearest(point, inData, inValues))[1]
            value = dimInterpolate(point, inData, inValues)
            writeFileData(output, value)

def polyInterpolation(datapoints, values, points, outputFile):
    #getting data
    output = open(outputFile, 'w')
    
    inData = fileToList(datapoints, 3)
    inData = np.array([np.array(x) for x in inData])
    
    inValues = fileToList(values, 3)
    inValues = np.array([np.array(x) for x in inValues])
        
    inPoints = fileToList(points, 3)
    inPoints = np.array([np.array(x) for x in inPoints])

    modelPoints = griddata(inData, inValues, inPoints, method = 'linear', rescale = True)
    for i in modelPoints:
        writeFileData(output, i)

polyInterpolation(RGBInput, RGBOutput, RGBPointsToCharacterize, 'polyInterpolation.txt')
print(error('polyInterpolation.txt', RGBBench))