import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import time
#import sklearn.preprocessing.polynomialfeatures

#visualize, say, r,g vs L, sweep through b.
#get the residulas for these too, this is more important

##OUR DATA##
CMYK1Bench = './Device_Characterization/CMYK-1/Benchmark//Output.txt'
CMYK1Characterization = './Device_Characterization/CMYK-1/Reference/Output.txt'

CMYK2Bench = './Device_Characterization/CMYK-2/Benchmark/Output.txt'
CMYK2Characterization = './Device_Characterization/CMYK-2/Reference/Output.txt'

RGBBench = './Device_Characterization/RGB-1/Benchmark/Output.txt'
RGBInput = './Device_Characterization/RGB-1/Characterization/Input.txt'
RGBOutput = './Device_Characterization/RGB-1/Characterization/Output.txt'
RGBPointsToCharacterize = './Device_Characterization//RGB-1/Reference/Input.txt'
RGBCharacterization = './Device_Characterization//RGB-1/Reference/Output.txt'

#######DATA HELPER FUNCTIONS#####################

#converts a file to a list
#of data points
def fileToList(file, dim):
    data = open(file, 'r')
    ret = []
    for line in data:
        point = line.rstrip('\n').split('\t')
        sub_ret = []
        for j in range(len(point)):
            try:
                sub_ret.append(float(point[j]))
            except:
                continue
        if len(sub_ret) == dim:
            ret.append(sub_ret)
    return np.array(ret)


def arraysToColoring(a, b, c):
    return np.array([(a[i]/255 , b[i]/255 , c[i]/255) for i in range(len(a))])

#RGB VALUES
l1 = fileToList(RGBPointsToCharacterize, 3)

#Lab VALUES
l2 = fileToList(RGBCharacterization, 3)

#Testing simple, no animation 3D plotting
Red = np.array([x[0]  for x in l1])
Green = np.array([x[1] for x in l1])
Blue = np.array([x[2] for x in l1])

Luminance = np.array([y[0] for y in l2])
A = np.array([y[1] for y in l2])
B = np.array([y[2] for y in l2])

plt.ion()
fig = plt.figure()
ax1 = fig.add_subplot(131, projection = '3d')
ax2 = fig.add_subplot(132, projection = '3d')
ax3 = fig.add_subplot(133, projection = '3d')

for i in range(10):
    ax1.scatter(Green, Blue, Luminance, marker = '.')
    ax2.scatter(Green, Blue, A, marker = '.')
    ax3.scatter(Green, Blue, B, marker = '.')
plt.show()






















